from django.urls import path


from source.webapp.views import nargiza, den_test_view, dev_ruslan , mariya, stas, ormon


urlpatterns = [
    path("", nargiza),
    path("ruslan/", dev_ruslan),
    path('den/', den_test_view),

    path('stas/', stas), 

    path('mariya/', mariya),
    path('ormon/', ormon),

 ]
