import http

from django.http import HttpResponse
from django.shortcuts import render
from django.template import loader


def nargiza(request):
    return render(request, 'index.html')


def dev_ruslan(request):
    if request.method == "GET":
        return render(
            request,
            "ruslan.html"
        )
    else:
        return http.HTTPStatus.NOT_FOUND


def den_test_view(request):
    template = loader.get_template('index.html')
    context = {
        'text': 'Hello, world!!!',
        'author': 'Денис Соколов'
    }
    return HttpResponse(template.render(context, request))



def stas(request):
    return HttpResponse('blabla')


def mariya(request):
    return render(request, 'mariya.html')

def ormon(request):
    return render(request, 'ormon.html')
